<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Strencrypt {
	var $CI;
    
	function Strencrypt() {
		$this->CI =& get_instance();
    }
	
	function parts($params='') {
		if(empty($params)) return FALSE;
		
		$parts = array();
		$params = str_replace(' ', '',$params);
		$parts = preg_split('/\-/si',$params);
		if(count($parts) < 2) {
			$parts = str_split($parts[0], 4);
			if(count($parts) < 4) return FALSE;
		}

		for($i=0;$i<4;$i++) {
			$parts[$i] = $this->strEncDec($parts[$i], false, 4, "fruhash".($i+1));
		}

		return $parts;
	}
	function hashkey($parts) {
		for($i=0;$i<5;$i++) {
			$parts[$i] = $this->CI->encrypt->encode($parts[$i]);
		}	
		
		return $parts;
	}
	function hashvalue($parts) {
		for($i=0;$i<5;$i++) {
			$parts[$i] = $this->CI->encrypt->decode($parts[$i]);
		}	
		return $parts;
	}	
	function keyvalue($parts) {
		for($i=0;$i<4;$i++) {
			$parts[$i] =  $this->strEncDec($parts[$i], true, 4, "fruhash".($i+1));
		}	
		return $parts;
	}	
	function datakey($data='', $dect=false, $key='fruhash5') {
		return $this->strEncDec($data, $dect, 4, $key);
	}
	
	function clientkey($data='', $dect=false, $key='clientkey') {
		return $this->strEncDec($data, $dect, 4, $key);
	}	
	
    
	function batchReverse($chash) {
		$parts = ($this->CI->util_model->getHash($chash));

		$parts = $this->CI->strencrypt->hashvalue($parts);
		$parts = $this->CI->strencrypt->keyvalue($parts);	
		$parts[4] = preg_replace('/x/','0',$parts[4]);
		
		$authinfo[] = $parts;
		
		if($parts[0][0] == 3) {
            $authinfo[] = str_pad(abs($parts[0]), 4, "0", STR_PAD_LEFT)  . str_pad(abs($parts[1]), 4, "0", STR_PAD_LEFT)  . str_pad(abs($parts[2]), 4, "0", STR_PAD_LEFT) . str_pad(abs($parts[3]), 3, "0", STR_PAD_LEFT);
		}
		else {
			$authinfo[] = str_pad(abs($parts[0]), 4, "0", STR_PAD_LEFT) . '-' . str_pad(abs($parts[1]), 4, "0", STR_PAD_LEFT) . '-' . str_pad(abs($parts[2]), 4, "0", STR_PAD_LEFT) . '-' . str_pad(abs($parts[3]), 4, "0", STR_PAD_LEFT);
		}
		$parts = str_split($parts[4], 2);
		$authinfo[] = $parts[0];
		$authinfo[] = $parts[1];
		return $authinfo;
	}
	
	function strEncDec($in, $to_num = false, $pad_up = false, $passKey = null){
	  $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	  if ($passKey !== null) {	 
		for ($n = 0; $n<strlen($index); $n++) {
		  $i[] = substr( $index,$n ,1);
		}
	 
		$passhash = hash('sha256',$passKey);
		$passhash = (strlen($passhash) < strlen($index)) ? hash('sha512',$passKey): $passhash;
	 
		for ($n=0; $n < strlen($index); $n++) {
		  $p[] =  substr($passhash, $n ,1);
		}
	 
		array_multisort($p,  SORT_DESC, $i);
		$index = implode($i);
	  }
	 
	  $base  = strlen($index);
	 
	  if ($to_num) {
		// Digital number  <<--  alphabet letter code
		$in  = strrev($in);
		$out = 0;
		$len = strlen($in) - 1;
		for ($t = 0; $t <= $len; $t++) {
		  $bcpow = bcpow($base, $len - $t);
		  $out   = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
		}
	 
		if (is_numeric($pad_up)) {
		  $pad_up--;
		  if ($pad_up > 0) {
			$out -= pow($base, $pad_up);
		  }
		}
		$out = sprintf('%F', $out);
		$out = substr($out, 0, strpos($out, '.'));
	  } else {
		// Digital number  -->>  alphabet letter code
		if (is_numeric($pad_up)) {
		  $pad_up--;
		  if ($pad_up > 0) {
			$in += pow($base, $pad_up);
		  }
		}
	 
		$out = "";
		for ($t = floor(log($in, $base)); $t >= 0; $t--) {
		  $bcp = bcpow($base, $t);
		  $a   = floor($in / $bcp) % $base;
		  $out = $out . substr($index, $a, 1);
		  $in  = $in - ($a * $bcp);
		}
		$out = strrev($out); // reverse
	  }
	 
	  return $out;
	}
}

?>